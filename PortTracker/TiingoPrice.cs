﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortTracker
{
    
    public class TiingoPrice
    {
        public double adjClose { get; set; }
        public double adjHigh { get; set; }
        public double adjLow { get; set; }
        public double adjOpen { get; set; }
        public int adjVolume { get; set; }
        public double close { get; set; }
        public DateTime date { get; set; }
        public double divCash { get; set; }
        public double high { get; set; }
        public double low { get; set; }
        public double open { get; set; }
        public double splitFactor { get; set; }
        public double volume { get; set; }
    }

    public class Root
    {
        public List<TiingoPrice> MyArray { get; set; }
    }

}
