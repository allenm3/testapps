﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace PortTracker
{
    public class TiingoApiClient
    {
        public static TiingoPrice GetDataForTicker(string Ticker)
        {
           
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri("https://api.tiingo.com/");
         
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    client.DefaultRequestHeaders.Add("Authorization", "token 5c776de5bdb6bb7490beb30bcd5ac7c8fdf866f8");

                    var responseTask = client.GetAsync(string.Format("tiingo/daily/{0}/prices", Ticker));
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        var readStr = result.Content.ReadAsStringAsync().Result;

                        //json response response from tingo always comes in side an array with one element. so just removing brackets
                        var tPrice = JsonConvert.DeserializeObject<TiingoPrice>(readStr.Replace("[", "").Replace("]", ""));
                        if (tPrice is null)
                            return null;
                        string resultStr = "";
                        resultStr += (tPrice.close + "@" + tPrice.date.ToString("yyyy-MM-dd HH:mm:ss"));

                        return tPrice;
                    }
                    else
                        return null;
                }
           
        }
    }
}
