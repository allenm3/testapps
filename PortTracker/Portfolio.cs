﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortTracker
{

    public class Portfolio
    {
        public List<Trade> PortfolioTrades;
        public Portfolio()
        {

        }
        
    }
    public class Trade
    {
        public Trade()
        {

        }
        public string Ticker { get; set; }
        public DateTime TradeDate { get; set; }
        public string TranType { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double Cost{get; set;}
    }
}
