﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortTracker
{
    public partial class Form1 : Form
    {
        IBindingList bData;
        BindingSource source;
        
        public Form1()
        {
            InitializeComponent();
            cmbTranType.SelectedIndex = 0;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
           try
            {
                var px = TiingoApiClient.GetDataForTicker(txtTicker.Text);
                if (px is null)
                {
                    MessageBox.Show("Invalid Ticker");
                    return;
                }
                lblMessage.Text = string.Format( "{0}@{1}", px.close.ToString(), px.date.ToString("yyyy-MM-dd HH:ss"));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check the ticker, might be invalid: " + ex.Message);
            }
            
           
            
        }


        /// <summary>
        /// when list changes (item added). read file, add item, write back to file
        /// triggered by source.Add(new Trade() { Ticker = "SDOW", Price = 100, TranType = "Buy" });
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Source_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                Portfolio data;
                if (e.ListChangedType == ListChangedType.ItemAdded)
                {
                    using (var streamReader = new StreamReader(@"port-trackerdata.json", Encoding.UTF8))
                    {
                        var json = streamReader.ReadToEnd();
                        data = JsonConvert.DeserializeObject<Portfolio>(json);
                        var newitem = ((BindingSource)sender)[e.NewIndex] as Trade;
                        data.PortfolioTrades.Add(newitem);
                        
                        
                    }
                    File.WriteAllText(@"port-trackerdata.json", JsonConvert.SerializeObject(data));
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error saving record to json file: " + ex.Message);
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            //load portfolioa data from json
            using (var streamReader = new StreamReader(@"port-trackerdata.json", Encoding.UTF8))
            {
                string json = streamReader.ReadToEnd();
                var data = JsonConvert.DeserializeObject<Portfolio>(json);
                bData = new BindingList<Trade>(data.PortfolioTrades);
                source = new BindingSource(bData, null);
                source.ListChanged += Source_ListChanged;

                gridPortfolio.AutoGenerateColumns = true;
                gridPortfolio.DataSource = source;

                

            }
        }

        private void btnAddTrade_Click(object sender, EventArgs e)
        {
            try
            {
                var px = TiingoApiClient.GetDataForTicker(txtAddTicker.Text);
                if (px is null)
                {
                    MessageBox.Show("Invalid Ticker");
                    return;
                }
                var newTrade = new Trade
                {
                    Ticker = txtAddTicker.Text.ToUpper(),
                    TradeDate = dtTradeDate.Value,
                    TranType = cmbTranType.Text,
                    Quantity = Convert.ToDouble(txtQty.Text),
                    Price=px.close,
                    Cost = Convert.ToDouble(txtQty.Text) * px.close
                };

                //add trade bindingsource will trigger listchanged and commit the trade to file
                source.Add(newTrade);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check trade fields, either invalid ticker or quantity: " + ex.Message);
            }
        }
    }
}
